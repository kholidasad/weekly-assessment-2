1. Internet works by using a packet routing network that follows Internet Protocol (IP) and Transport Control Protocol (TCP). When data is transferred over the internet, data sent over the internet is called a message, but before a message get sent, they are broken up into tinier parts called packets. The Transport Control Protocol (TCP) works with IP to ensure transfer of data is dependable and reliable. This helps to make sure that no packets are lost, packets are reassembled in proper sequence, and there’s no delay negatively affecting the data quality. 
    Example : 
    a. Your devices connected to the web using a modem or router. Together, these devices allow you to connect to the other network around the globe using Internet Service Provider (ISP).
    b. You type in a web address, known as a URL (Uniform Resource Locator). Each website has its own unique URL that signals to your ISP where you want to go.
    c. Your query is pushed to your ISP which connects to several servers which store and send data like a NAP Server (Network Access Protection) and a DNS (Domain Name Server).
    d. Your browser sends a Hypertext Transfer Protocol (HTTP) request to the target server to send a copy of the website to the client using TCP/IP.
    e. The server then approves request and sends a “200 OK” message to your computer. Then, the server sends website files to the browser in the form of data packets.
    f. As your browser reassembles the data packets, the website loads allowing you to learn, shop, browse, and engage.

2. Communication protocol is a protocol that allow two or more entities of communication system to connect and exchange data with each other.

3. HTTP status codes itself is a server response to browser's request. All HTTP status codes divide into 5 categories. The first digit of the status codes define the class of response. There are 5 values for the first digit : 
    a. 1xx (Informational) => the request was received, continuing process,
    b. 2xx (Successfull) => the request was successfully received, understood, and accepted,
    c. 3xx (Redirection) => further actions need to be taken in order to complete the request,
    d. 4xx (Client Error) => the request contain bad syntax or cannot be fulfilled,
    e. 5xx (Server Error) => the server failed to fulfill an apparently valid request

4. When you go to a restaurant, there's a the dining room which is the main restaurant where a clients sit down, a serving staff brings the menu, the client reads, makes an order, then the serving staff send that order to the kitchen, the kitchen response about the order, the kitchen makes food, after the food is done it taken by the serving staff to the clients, and their food is served.

5. There are several HTTP method, which is: 
    a. GET => to retrieve information from given server using a given URI. Requests using GET only retrieve data and should not affect on the data.
    b. POST => to send data to the server, (exp: like file upload or HTML forms)
    c. PUT => replaces all current representations of the target resource with the uploaded content
    d. PATCH => method that applies partial modifications to resource
    e. DELETE => removes all current representations of the target resource using given URI
    f. HEAD => same as GET method, but without the response body
    g. CONNECT => to establishes a tunnel to the server identified by target resource
    h. OPTION => to describe the communication options for the target resources
    i. TRACE => to perform a message loop-back test along the path to the target resource